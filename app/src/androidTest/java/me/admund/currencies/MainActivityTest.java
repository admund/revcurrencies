package me.admund.currencies;

import org.junit.Test;

import androidx.lifecycle.Lifecycle;

import androidx.test.core.app.ActivityScenario;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.StringContains.containsString;

public class MainActivityTest {

    @Test
    public void changeRatioValueTo100() {
        ActivityScenario<MainActivity> activityScenario = ActivityScenario.launch(MainActivity.class);

        activityScenario.moveToState(Lifecycle.State.RESUMED);

        onView(withId(R.id.currencyET))
                .perform(clearText())
                .check(matches(withText("")));

        onView(withId(R.id.currencyET))
                .perform(typeText("100"))
                .check(matches(withText("100")));

        activityScenario.moveToState(Lifecycle.State.DESTROYED);
    }

    @Test
    public void changeRatioValueTo1Dot234() {
        ActivityScenario<MainActivity> activityScenario = ActivityScenario.launch(MainActivity.class);

        activityScenario.moveToState(Lifecycle.State.RESUMED);

        onView(withId(R.id.currencyET))
                .perform(clearText())
                .check(matches(withText("")));

        onView(withId(R.id.currencyET))
                .perform(typeText("1.2345"))
                .check(matches(withText("1.23")));

        activityScenario.moveToState(Lifecycle.State.DESTROYED);
    }

    @Test
    public void changeRatioValueTo123456789() {
        ActivityScenario<MainActivity> activityScenario = ActivityScenario.launch(MainActivity.class);

        activityScenario.moveToState(Lifecycle.State.RESUMED);

        onView(withId(R.id.currencyET))
                .perform(clearText())
                .check(matches(withText("")));

        onView(withId(R.id.currencyET))
                .perform(typeText("123456789"))
                .check(matches(withText("123456")));

        activityScenario.moveToState(Lifecycle.State.DESTROYED);
    }

    @Test
    public void waitForData() throws InterruptedException {
        ActivityScenario<MainActivity> activityScenario = ActivityScenario.launch(MainActivity.class);

        activityScenario.moveToState(Lifecycle.State.RESUMED);

        onView(withId(R.id.ratesListView))
                .check(matches(not(hasDescendant(withText(containsString("AUD"))))));

        Thread.sleep(3000);

        onView(withId(R.id.ratesListView))
                .check(matches(hasDescendant(withText(containsString("AUD")))));

        activityScenario.moveToState(Lifecycle.State.DESTROYED);
    }

    @Test
    public void clickOnListViewItem() throws InterruptedException {
        ActivityScenario<MainActivity> activityScenario = ActivityScenario.launch(MainActivity.class);

        activityScenario.moveToState(Lifecycle.State.RESUMED);

        Thread.sleep(3000);

        onView(withId(R.id.ratesListView))
                .check(matches(hasDescendant(withText(containsString("AUD")))));

        onData(anything())
                .inAdapterView(withId(R.id.ratesListView))
                .atPosition(0)
                .perform(click());

        onView(withId(R.id.ratesListView))
                .check(matches(not(hasDescendant(withText(containsString("AUD"))))));

        activityScenario.moveToState(Lifecycle.State.DESTROYED);
    }
}
