package android.util;

public class Log {

    public static int v(String tag, String message) {
        System.out.println("VERBOSE: " + tag + " : " + message);
        return 0;
    }

    public static int d(String tag, String message) {
        System.out.println("DEBUG: " + tag + " : " + message);
        return 0;
    }

    public static int i(String tag, String message) {
        System.out.println("INFO: " + tag + " : " + message);
        return 0;
    }

    public static int w(String tag, String message) {
        System.out.println("WARRN : " + tag + " : " + message);
        return 0;
    }

    public static int e(String tag, String message) {
        System.out.println("ERROR: " + tag + " : " + message);
        return 0;
    }
}
