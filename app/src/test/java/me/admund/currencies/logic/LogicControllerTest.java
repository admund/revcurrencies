package me.admund.currencies.logic;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.net.UnknownHostException;

import me.admund.currencies.rev.RevCurrencies;
import me.admund.currencies.rev.RevCurrenciesInterface;
import me.admund.currencies.ui.IInternetConnectionListener;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class LogicControllerTest {

    private static final String EUR = "EUR";

    @Mock
    private RevCurrenciesInterface revCurrenciesInterfaceMock;

    @Mock
    private IRevCurrenciesListener revCurrenciesListenerMock;

    @Mock
    private IInternetConnectionListener internetConnectionListener;

    private LogicController tested;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        tested = new LogicController(revCurrenciesInterfaceMock, revCurrenciesListenerMock, internetConnectionListener);
    }

    @Test
    public void checkIfListenerExecuteOnGetRevCurrenciesMethodWhenRevCurrenciesIsNotNull() throws InterruptedException {
        RevCurrencies revCurrenciesMock = mock(RevCurrencies.class);
        Call<RevCurrencies> callMock = createCallMock(revCurrenciesMock, ExecuteResult.SUCCESS);
        when(revCurrenciesInterfaceMock.currencies(EUR)).thenReturn(callMock);

        tested.getCurrencies(EUR);

        Thread.sleep(100);

        verify(revCurrenciesListenerMock).onGetRevCurrencies(revCurrenciesMock);
        verifyNoMoreInteractions(revCurrenciesListenerMock);
        verify(revCurrenciesInterfaceMock).currencies(EUR);
        verifyNoMoreInteractions(revCurrenciesInterfaceMock);
        verifyNoMoreInteractions(internetConnectionListener);
    }

    @Test
    public void checkIfListenerNotExecuteOnGetRevCurrenciesMethodWhenRevCurrenciesIsNull() throws InterruptedException {
        Call<RevCurrencies> callMock = createCallMock(null, ExecuteResult.SUCCESS);
        when(revCurrenciesInterfaceMock.currencies(EUR)).thenReturn(callMock);

        tested.getCurrencies(EUR);

        Thread.sleep(100);

        verifyNoMoreInteractions(revCurrenciesListenerMock);
        verify(revCurrenciesInterfaceMock).currencies(EUR);
        verifyNoMoreInteractions(revCurrenciesInterfaceMock);
        verifyNoMoreInteractions(internetConnectionListener);
    }

    @Test
    public void checkIfListenerNotExecuteOnGetRevCurrenciesMethodWhenExecuteThrowIOException() throws InterruptedException {
        Call<RevCurrencies> callMock = createCallMock(null, ExecuteResult.IO_EXCEPTION);
        when(revCurrenciesInterfaceMock.currencies(EUR)).thenReturn(callMock);

        tested.getCurrencies(EUR);

        Thread.sleep(100);

        verifyNoMoreInteractions(revCurrenciesListenerMock);
        verify(revCurrenciesInterfaceMock).currencies(EUR);
        verifyNoMoreInteractions(revCurrenciesInterfaceMock);
        verifyNoMoreInteractions(internetConnectionListener);
    }

    @Test
    public void checkIfListenerNotExecuteOnGetRevCurrenciesMethodWhenExecuteThrowUnknownException() throws InterruptedException {
        Call<RevCurrencies> callMock = createCallMock(null, ExecuteResult.UNKNOWN_HOST_EXCEPTION);
        when(revCurrenciesInterfaceMock.currencies(EUR)).thenReturn(callMock);

        tested.getCurrencies(EUR);

        Thread.sleep(100);

        verifyNoMoreInteractions(revCurrenciesListenerMock);
        verify(revCurrenciesInterfaceMock).currencies(EUR);
        verifyNoMoreInteractions(revCurrenciesInterfaceMock);
        verify(internetConnectionListener).onUnknownHostException();
        verifyNoMoreInteractions(internetConnectionListener);
    }

    enum ExecuteResult {SUCCESS, IO_EXCEPTION, UNKNOWN_HOST_EXCEPTION}

    private Call<RevCurrencies> createCallMock(RevCurrencies revCurrenciesMock, ExecuteResult executeResult) {
        return new Call<RevCurrencies>() {
            @Override
            public Response<RevCurrencies> execute() throws IOException {
                if (executeResult == ExecuteResult.IO_EXCEPTION) {
                    throw new IOException();
                } else if (executeResult == ExecuteResult.UNKNOWN_HOST_EXCEPTION) {
                    throw new UnknownHostException();
                }

                return Response.success(revCurrenciesMock);
            }

            @Override
            public void enqueue(Callback<RevCurrencies> callback) {

            }

            @Override
            public boolean isExecuted() {
                return false;
            }

            @Override
            public void cancel() {

            }

            @Override
            public boolean isCanceled() {
                return false;
            }

            @Override
            public Call<RevCurrencies> clone() {
                return this;
            }

            @Override
            public Request request() {
                return new Request.Builder().build();
            }
        };
    }
}
