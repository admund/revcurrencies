package me.admund.currencies.logic;

public interface ILogicController {
    void getCurrencies(String countryShort);
}
