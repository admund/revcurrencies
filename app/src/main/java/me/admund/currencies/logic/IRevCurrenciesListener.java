package me.admund.currencies.logic;

import me.admund.currencies.rev.RevCurrencies;

public interface IRevCurrenciesListener {
    void onGetRevCurrencies(RevCurrencies revCurrencies);
}
