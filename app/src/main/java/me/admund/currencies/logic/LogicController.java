package me.admund.currencies.logic;

import android.util.Log;

import java.io.IOException;
import java.net.UnknownHostException;

import me.admund.currencies.rev.RevCurrenciesInterface;
import me.admund.currencies.rev.RevCurrencies;
import me.admund.currencies.ui.IInternetConnectionListener;
import retrofit2.Call;
import retrofit2.Response;

public class LogicController implements ILogicController {

    private static final String TAG = LogicController.class.getSimpleName();

    private RevCurrenciesInterface currenciesInterface;
    private IRevCurrenciesListener revCurrenciesListener;
    private IInternetConnectionListener internetConnectionListener;

    public LogicController(RevCurrenciesInterface currenciesInterface, IRevCurrenciesListener revCurrenciesListener,
                           IInternetConnectionListener internetListener) {
        this.currenciesInterface = currenciesInterface;
        this.revCurrenciesListener = revCurrenciesListener;
        this.internetConnectionListener = internetListener;
    }

    @Override
    public void getCurrencies(String countryShort) {
        new Thread() {
            @Override
            public void run() {
                getCurrenciesRun(countryShort);
            }
        }.start();
    }

    private void getCurrenciesRun(String countryShort) {
        Call<RevCurrencies> currencies = currenciesInterface.currencies(countryShort);
        Response<RevCurrencies> response;
        try {
            response = currencies.execute();
        } catch (IOException ioe) {
            Log.e(TAG, ioe + " message: " + ioe.getMessage());
            if (ioe instanceof UnknownHostException) {
                internetConnectionListener.onUnknownHostException();
            }
            return;
        }

        if (response.isSuccessful()) {
            RevCurrencies revCurrencies = response.body();
            if (revCurrencies != null) {
                if (revCurrenciesListener != null) {
                    revCurrenciesListener.onGetRevCurrencies(revCurrencies);
                }
                return;
            }
        }
        Log.e(TAG, "Can't get currencies respond code: " + response.code());
    }
}
