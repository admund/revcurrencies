package me.admund.currencies.rev;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RevCurrenciesInterface {
    @GET("latest")
    Call<RevCurrencies> currencies(@Query("base") String base);
}
