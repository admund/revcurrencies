package me.admund.currencies.rev;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class RevCurrencies {
    @SerializedName("base")
    public String base;
    @SerializedName("date")
    public String data;
    @SerializedName("rates")
    public Map<String, Float> rates;
}
