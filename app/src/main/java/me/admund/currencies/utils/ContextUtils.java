package me.admund.currencies.utils;

import android.content.Context;
import android.content.res.Resources;

import me.admund.currencies.R;

public class ContextUtils {

    private Context context;

    public ContextUtils(Context context) {
        this.context = context;
    }

    public int getFlagDrawableResId(String countryShort) {
        Resources resources = context.getResources();
        final int resourceId = resources.getIdentifier(countryShort.toLowerCase(), "drawable", context.getPackageName());
        return resourceId == 0 ? R.drawable.no_flag : resourceId;
    }

    public String getCountryDescriptionResId(String countryShort) {
        Resources resources = context.getResources();
        final int resourceId = resources.getIdentifier(countryShort.toLowerCase(), "string", context.getPackageName());
        return resourceId == 0 ? countryShort : resources.getString(resourceId);
    }
}
