package me.admund.currencies;

import androidx.appcompat.app.AppCompatActivity;
import me.admund.currencies.logic.ILogicController;
import me.admund.currencies.logic.LogicController;
import me.admund.currencies.rev.RevCurrenciesInterface;
import me.admund.currencies.ui.IInternetConnectionListener;
import me.admund.currencies.ui.RateModel;
import me.admund.currencies.ui.RateViewHolder;
import me.admund.currencies.ui.RatesListViewAdapter;
import me.admund.currencies.utils.ContextUtils;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements IInternetConnectionListener {

    private static final String API_URL = "https://revolut.duckdns.org";
    private static final int MAX_NUMBER = 1_000_000;
    private static final int GET_CURRENCIES_INTERVAL_MS = 1_000;

    private ILogicController logic;
    private RatesListViewAdapter adapter;

    private RateViewHolder mainCurrency;

    private Timer getCurrentTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();

        setContentView(R.layout.activity_main);

        Retrofit builder = new Retrofit.Builder()
            .baseUrl(API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

        // adpater
        adapter = new RatesListViewAdapter(this, new ContextUtils(getApplicationContext()));
        logic = new LogicController(builder.create(RevCurrenciesInterface.class), adapter, this);

        // listView
        ListView ratesListView = findViewById(R.id.ratesListView);
        ratesListView.setAdapter(adapter);
        ratesListView.setOnItemClickListener((parent, view, position, id) -> {
            RateModel model = (RateModel) adapter.getItem(position);
            updateMainCurrencyView(model);
            changeMainCurrency(model.countryShort);
        });

        // main currency
        View mainCurrencyLayout = findViewById(R.id.mainCurrencyLayout);
        mainCurrency = new RateViewHolder(mainCurrencyLayout.findViewById(R.id.countryFlagIV),
            mainCurrencyLayout.findViewById(R.id.countryShortTV),
            mainCurrencyLayout.findViewById(R.id.countryLongTV),
            mainCurrencyLayout.findViewById(R.id.currencyET));
        mainCurrency.valueET.addTextChangedListener(new RevTextWatcher());
        mainCurrency.valueET.setFilters(new InputFilter[]{new MaxNumberInputFilter()});
    }

    @Override
    protected void onStart() {
        super.onStart();

        createGetCurrentTimer();
    }

    @Override
    protected void onStop() {
        super.onStop();

        cancelGetCurrentTimer();
    }

    @Override
    public void onUnknownHostException() {
        runOnUiThread(() -> Toast.makeText(getApplicationContext(), R.string.check_internet, Toast.LENGTH_SHORT).show());
    }

    private void changeMainCurrency(String countryShort) {
        logic.getCurrencies(countryShort);
    }

    private void updateMainCurrencyView(RateModel model) {
        mainCurrency.countryFlagIV.setImageResource(model.flagImageResId);
        mainCurrency.countryShortTV.setText(model.countryShort);
        mainCurrency.countryLongTV.setText(model.countryDesc);
        float newValue = model.rate * adapter.getMultiplier();
        mainCurrency.valueET.setText(String.format(Locale.getDefault(), "%.2f", newValue > MAX_NUMBER ? MAX_NUMBER : newValue));
    }

    private void createGetCurrentTimer() {
        if (getCurrentTimer != null) {
            cancelGetCurrentTimer();
        }

        getCurrentTimer = new Timer();
        getCurrentTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                logic.getCurrencies(mainCurrency.countryShortTV.getText().toString());
            }
        }, GET_CURRENCIES_INTERVAL_MS, GET_CURRENCIES_INTERVAL_MS);
    }

    private void cancelGetCurrentTimer() {
        if (getCurrentTimer != null) {
            getCurrentTimer.cancel();
            getCurrentTimer = null;
        }
    }

    private class MaxNumberInputFilter implements InputFilter {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int c) {
            String result = String.format("%s%s%s", dest.subSequence(0, dstart), source, dest.subSequence(dstart, dest.length()));
            if (moreThenTwoAfterDot(result)) {
                return "";
            }
            try {
                if (Float.parseFloat(result) > MAX_NUMBER) {
                    return "";
                }
            } catch (NumberFormatException e) {
                return "";
            }

            return null;
        }

        private boolean moreThenTwoAfterDot(String textToCheck) {
            String[] splitTab = textToCheck.split("\\.");
            if (splitTab.length < 2) {
                return false;
            }

            return splitTab[1].length() > 2;
        }
    }

    private class RevTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // do nothing
        }

        @Override
        public void onTextChanged(CharSequence newText, int start, int before, int count) {
            //do nothing

            float newMultiplier = newText.length() > 0 ? Float.parseFloat(newText.toString()) : 0;
            adapter.changeMultiplier(newMultiplier);
        }

        @Override
        public void afterTextChanged(Editable s) {
            // do nothing
        }
    }
}
