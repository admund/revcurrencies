package me.admund.currencies.ui;

public interface IInternetConnectionListener {
    void onUnknownHostException();
}
