package me.admund.currencies.ui;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import me.admund.currencies.R;
import me.admund.currencies.logic.IRevCurrenciesListener;
import me.admund.currencies.rev.RevCurrencies;
import me.admund.currencies.utils.ContextUtils;

public class RatesListViewAdapter extends BaseAdapter implements IRevCurrenciesListener {

    private final Activity activity;
    private final ContextUtils utils;
    private final List<RateModel> rateModelList = new ArrayList<>();
    private float multiplier = 100;

    public RatesListViewAdapter(Activity activity, ContextUtils utils) {
        this.activity = activity;
        this.utils = utils;
    }

    @Override
    public int getCount() {
        return rateModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return rateModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RateViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.currency_layout, null);
            holder = new RateViewHolder(convertView.findViewById(R.id.countryFlagIV),
                convertView.findViewById(R.id.countryShortTV),
                convertView.findViewById(R.id.countryLongTV),
                convertView.findViewById(R.id.currencyET));
            holder.valueET.setEnabled(false);
            convertView.setTag(holder);
        }
        holder = (RateViewHolder) convertView.getTag();

        RateModel model = rateModelList.get(position);
        holder.countryFlagIV.setImageResource(model.flagImageResId);
        holder.countryShortTV.setText(model.countryShort);
        holder.countryLongTV.setText(model.countryDesc);
        holder.valueET.setText(String.format(Locale.getDefault(), "%.2f", model.rate * multiplier));

        return convertView;
    }

    @Override
    public void onGetRevCurrencies(RevCurrencies revCurrencies) {
        activity.runOnUiThread(() -> {

            rateModelList.clear();
            for (Map.Entry<String, Float> entry : revCurrencies.rates.entrySet()) {
                RateModel model = new RateModel();
                model.countryShort = entry.getKey();
                model.flagImageResId = utils.getFlagDrawableResId(model.countryShort);
                model.countryDesc = utils.getCountryDescriptionResId(model.countryShort);
                model.rate = entry.getValue();
                rateModelList.add(model);
            }

            notifyDataSetChanged();
        });
    }

    public void changeMultiplier(float newMultiplier) {
        this.multiplier = newMultiplier;

        activity.runOnUiThread(this::notifyDataSetChanged);
    }

    public float getMultiplier() {
        return multiplier;
    }
}
