package me.admund.currencies.ui;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class RateViewHolder {

    public RateViewHolder(ImageView countryFlagIV, TextView countryShortTV, TextView countryLongTV, EditText valueET) {
        this.countryFlagIV = countryFlagIV;
        this.countryShortTV = countryShortTV;
        this.countryLongTV = countryLongTV;
        this.valueET = valueET;
    }

    public final ImageView countryFlagIV;
    public final TextView countryShortTV;
    public final TextView countryLongTV;
    public final EditText valueET;
}